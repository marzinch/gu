# Site web des élu·e·s étudiant·e·s 2019-2021

État du site: [![pipeline status](https://gitlab.utc.fr/elus_etu/gu/badges/master/pipeline.svg)](https://gitlab.utc.fr/elus_etu/gu/-/commits/master)

Ce site est le votre, toute contribution est la bienvenue ! Pas besoin de compétences techniques, vous pouvez l'éditer directement depuis votre navigateur :)

Pour cela, suivez le [tutoriel en 3 simples étapes](doc/tutoriel.md) 🚀

Vous pouvez également consulter les articles techniques suivants:

* [Résumé du fonctionnement du CD (_Continuous Deployment_)](doc/fonctionnement_cd.md)
* [Développement en local avec Docker](doc/dev_docker.md)
* [Monter un site similaire avec GitLab](doc/site_bis.md)

## Crédits

Ce site est hébergé sur l'instance GitLab de l'UTC en utilisant GitLab Pages. Il est généré par [Jekyll](https://jekyllrb.com/).

La mise en place initiale a été menée par Andrés Maldonado, à partir du thème [Indigo](https://github.com/sergiokopplin/indigo) de [Sérgio Kopplin](https://koppl.in/) (+38 contributeurs), disponible sous licence [MIT](doc/original_license.txt).

## Licences

[![GNU AGPLv3 Image](/assets/images/doc/AGPLv3.png)](http://www.gnu.org/licenses/agpl-3.0.fr.html)

[![Creative Commons Attribution-ShareAlike 4.0 International Image](/assets/images/doc/CC_BY-SA_4.0.png)](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)  

Le code contenu dans ce dépôt utilise la licence [GNU AGPLv3](http://www.gnu.org/licenses/agpl-3.0.fr.html).

Sauf mention contraire, les posts eux mêmes ainsi que les images sont sous licence [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

[Plus d'infos sur le choix des licences](/doc/licences.md)
