J'ai passé un moment à réfléchir sur la licence idéale pour un site comme celui-ci, et à m'informer sur les options existantes.

Le thème [Indigo](https://github.com/sergiokopplin/indigo), sur lequel ce site est basé, est sous une licence très permissive: la licence [MIT](/doc/original_license.txt). Cependant, je souhaitais une licence à [copyleft](https://fr.wikipedia.org/wiki/Copyleft) fort, qui éviterait que tout le travail réalisé tombe dans le propriétaire.

Le problème est qu'aucune des licences à copyleft fort existantes n'est vraiment adaptée à notre cas. En effet, notre site contient des posts et des images, mais aussi du code qui permet de le générer automatiquement.

Pour le contenu multimédia, les [licences Creative Commons](https://creativecommons.org) sont un choix populaire, et la [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) est une licence à copyleft fort. Cependant, [les licences Creative Commons sont découragées pour les logiciels](https://creativecommons.org/faq/#can-i-apply-a-creative-commons-license-to-software).

D'un autre côté, les [licences GNU](https://www.gnu.org/licenses/licenses.fr.html) sont un très bon choix pour les logiciels, mais ne sont pas idéales pour du contenu multimédia, puisqu'il n'y a pas toujours de distinction entre «forme source» et «forme compilée» ([plus d'infos](https://opensource.stackexchange.com/questions/6388/how-does-gpl-work-for-articles-on-a-website?rq=1)). De plus, elles exigent d'inclure une copie de la licence avec tout contenu partagé, ce qui serait très vite décourageant si quelqu'un veut partager un post par exemple.

Finalement, j'ai décidé d'utiliser l'[AGPLv3](http://www.gnu.org/licenses/agpl-3.0.fr.html) pour le code et la [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) pour les posts et les images. L'AGPLv3 est quasiment identique à la GPLv3, mais elle contient « une clause additionnelle qui autorise les utilisateurs interagissant avec le logiciel sous licence via un réseau à recevoir les sources de ce programme », ce qui fait sens pour le cas de notre site.

## Liens intéressants

* https://drewdevault.com//2019/06/13/My-journey-from-MIT-to-GPL.html
* https://blog.tidelift.com/the-state-of-copyleft-licensing
* https://opensource.stackexchange.com/questions/6388/how-does-gpl-work-for-articles-on-a-website
